lfs-bst
=======

Linux from Scratch with BuildStream.

This project is a run-through of the excellent ["Linux from Scratch
book"](http://www.linuxfromscratch.org/lfs/view/stable-systemd/), by
Gerard Beekmans, licensed under [CC BY-NC-SA
2.0](http://www.linuxfromscratch.org/lfs/view/stable-systemd/appendices/creat-comm.html).

We use the meta-build system 'BuildStream' to do sandboxed builds, so
everything is more repeatable. This is also a good test for BuildStream.

The BuildStream project is available here:
https://gitlab.com/BuildStream/buildstream

Stage 0 - a host platform
-------------------------

We must rely on a host system to build the tools needed to build the LFS system.

This corresponds to [chapter 2 of the LFS
book](http://www.linuxfromscratch.org/lfs/view/stable-systemd/chapter02/chapter02.html).

Here we will use Docker to prepare a system with the requisite tools, and then
export it to a tar file. When building in BuildStream, we will use this tar file
as the host platform for all the initial build steps.

Note that you will need Docker and BuildStream installed to proceed.

First, use Docker to create the tar file:

```bash
# Make sure you're at the repo root before running.
cd s0
./make_host_platform.sh
# It took roughly 20 minutes to complete this on my machine.
```

Next, prepare the `host_platform.bst` file. Note that we assume you are
using `bst-here` to invoke BuildStream. If you are not, you'll need to fix up
the path to `host_platform.tar` and adjust the steps below accordingly.

```
# Make sure youre at the repo root.
cp s0/host_platform.bst{.template,}
bst-here track s0/host_platform.bst
bst-here build s0/host_platform.bst
```

Note that `s0/host_platform.bst` is in `.gitignore`. This is because everyone
will have a generated a tar file with different contents, so the ref will not
be stable. We avoid accidental meaningless changes by separating out the
important bits into template to copy. The trade-off is that if we need to
change the template, folks won't automatically get an updated
`host_platform.bst`.

Then, check that the versions of the tools we have are fit for our purposes.

```bash
# Make sure you're at the repo root before running.
bst-here build s0/host_platform_version_list.bst
bst-here checkout s0/host_platform_version_list.bst . --force

cat versions.txt
# Please manually verify that the versions are as required at:
# http://www.linuxfromscratch.org/lfs/view/stable-systemd/chapter02/hostreqs.html

rm versions.txt
```

Stage 1 - A Temporary System - Pass 1
-------------------------------------

This corresponds to the first part of [chapter 5 of the LFS
book](http://www.linuxfromscratch.org/lfs/view/stable-systemd/chapter05/chapter05.html).

This is where we'll get to the point where we can compile a basic program using
the cross-compiler we have built.

```bash
bst-here build s1/test.bst
```

Stage 2 - A Temporary System - Pass 2
-------------------------------------

This corresponds to the second pass in [chapter 5 of the LFS
book](http://www.linuxfromscratch.org/lfs/view/stable-systemd/chapter05/chapter05.html).

This is where we'll get to the point where we can compile a basic program using
the cross-compiled compiler we have built. It builds binaries that rely only on
the /tools directory at runtime.

```bash
bst-here build s2/test.bst
```

Stage 3 - A Temporary System - The Tools
----------------------------------------

This corresponds to the rest of [chapter 5 of the LFS
book](http://www.linuxfromscratch.org/lfs/view/stable-systemd/chapter05/chapter05.html).

This is where we'll get to the point where we have a self-sufficient /tools
directory, which can be chrooted into. It has the tools we'll need to build the
rest of the system. We'll then be independent from the host platform created in
stage 0.

```bash
bst-here build s3/tools.bst
```
