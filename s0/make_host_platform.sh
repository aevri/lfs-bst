set -x
docker build host_platform -t lfs_host_platform &&
    docker create --name lfs_host_platform lfs_host_platform false &&
    docker export lfs_host_platform --output host_platform.tar &&
    docker rm lfs_host_platform &&
    mkdir -v host_platform.tmp &&
    cd host_platform.tmp &&
    tar x --exclude 'dev/*' -f ../host_platform.tar &&
    rm -rf tmp dev proc &&
    rm -f etc/*shadow* &&
    rm ../host_platform.tar &&
    tar cf ../host_platform.tar . &&
    chmod u+w root usr/* usr/lib64/* &&
    cd .. &&
    rm -rf host_platform.tmp ||
    echo Failed
